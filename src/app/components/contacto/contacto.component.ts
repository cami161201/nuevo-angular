import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup,Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.css']
})
export class ContactoComponent implements OnInit {
forma!:FormGroup;
//,private _snackbar:MatSnackBar
  constructor(private fb:FormBuilder,private _snackbar:MatSnackBar) {
    this.crearFormulario();
   // this.cargarDatos();
   }
   crearFormulario():void{
    this.forma = this.fb.group({
      nombre:['',[Validators.required,Validators.minLength(4),Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
      correo:['',[Validators.required,Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
      mensaje:['',[Validators.required,Validators.minLength(6)]]
    });
   }

   guardar():void{
    if(!this.forma.valid){
      return
    }
    console.log(this.forma.value);

    //reset del formulario
    this.LimpiarFormulario();
    this._snackbar.open('Email enviado',' ',{
      duration:1500,
      horizontalPosition:'center',
      verticalPosition:'bottom'
    });
   }

   get nombreNoValido(){
    return this.forma.get('nombre')?.invalid && this.forma.get('nombre')?.touched;
   }

   get correoNoValido(){
    return this.forma.get('correo')?.invalid && this.forma.get('correo')?.touched;
   }

   get mensajeNoValido(){
    return this.forma.get('mensaje')?.invalid && this.forma.get('mensaje')?.touched;
   }
  //  cargarDatos():void{
  //   this.forma.setValue({
  //     nombre : 'Camila',
  //     correo:'camilamo@gmail.com',
  //     mensaje:'Algun mensaje ha de haber'
  //   })
  //  }

   LimpiarFormulario():void{
    this.forma.reset({
      nombre:'',
      correo:'',
      mensaje:''
    })
   }
  ngOnInit(): void {
  }

}
